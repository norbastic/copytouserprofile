﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace copytostartup
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args.Length == 0)
            {
                Console.WriteLine("Usage: please specify the script which you want to copy to the autostart folders.");
                Environment.Exit(1);
            }
            else if(args.Length == 1)
            {
                if(args[0].Length < 5)
                {
                    Console.WriteLine("Is that a real filename?");
                    Environment.Exit(1);
                }
                else if (!File.Exists(args[0]))
                {
                    Console.WriteLine("File does not exist.");
                    Environment.Exit(1);
                }
            }

            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_UserProfile WHERE Loaded = True");

                var sourceFile = args[0];

                foreach (var item in searcher.Get())
                {
                    var userProfile = item.GetPropertyValue("LocalPath");
                    var destinationPath = Path.Combine(userProfile.ToString(), @"AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup", Path.GetFileName(sourceFile));
                    try
                    {
                        File.Copy(sourceFile, destinationPath);
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Profile folder does not exist. Or access was denied.");                                                
                    }                                                     

                }

            }
            catch (Exception)
            {
                Environment.Exit(0);
            }
        }
    }
}
